#include <stdio.h>
#include <stdlib.h>
#ifndef LINKEDLIST_H_
#define LINKEDLIST_H_
typedef struct node {
	int data ;
	struct node* next ;
}Node;

void add (Node ** n , int data) ;
void removen(Node ** n , int index) ;
int Access(Node ** n , int index) ;
int length (Node ** n) ;
#endif
